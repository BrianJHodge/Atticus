/* Copyright 2004-2018 Game Slayer Studios */
/*           All Rights Reserved           */

#include <SparkFun_TB6612.h>
#include <EnableInterrupt.h>
 
/* Begin Motor Setup */
#define AIN1 9
#define BIN1 11
#define AIN2 8
#define BIN2 12
#define PWMA 3
#define PWMB 5
#define STBY 13
 
// these constants are used to allow you to make your motor configuration
// line up with function names like forward.  Value can be 1 or -1
const int offsetA = 1;
const int offsetB = 1;
 
Motor motor1 = Motor(AIN1, AIN2, PWMA, offsetA, STBY);
Motor motor2 = Motor(BIN1, BIN2, PWMB, offsetB, STBY);
 
/* End Motor Setup */
 
/* Begin Receiver Setup */
 
#define SERIAL_PORT_SPEED 57600
#define RC_NUM_CHANNELS 2
 
#define RC_CH1 0 //Steering   L 988 - 1980 R    C: 1480/1484
#define RC_CH2 1 //Throttle   F 1000 - 1564 B   C: 1500/1504
 
#define RC_CH1_DZ 10
#define RC_CH2_DZ 20
 
#define RC_CH1_INPUT A0
#define RC_CH2_INPUT A1
 
uint16_t rc_values[RC_NUM_CHANNELS];
uint32_t rc_start[RC_NUM_CHANNELS];
volatile uint16_t rc_shared[RC_NUM_CHANNELS];
 
void calc_ch1() { calc_input(RC_CH1, RC_CH1_INPUT); }
void calc_ch2() { calc_input(RC_CH2, RC_CH2_INPUT); }
 
void calc_input(uint8_t channel, uint8_t input_pin) {
  if (digitalRead(input_pin) == HIGH) {
    rc_start[channel] = micros();
  } else {
    uint16_t rc_compare = (uint16_t)(micros() - rc_start[channel]);
    rc_shared[channel] = rc_compare;
  }
}
 
void rc_read_values() {
  noInterrupts();
  memcpy(rc_values, (const void *)rc_shared, sizeof(rc_shared));
  interrupts();
}
 
/* End Receiver Setup */
 
void setup(){
 
  Serial.begin(SERIAL_PORT_SPEED);
 
  pinMode(RC_CH1_INPUT, INPUT);
  pinMode(RC_CH2_INPUT, INPUT);
 
  enableInterrupt(RC_CH1_INPUT, calc_ch1, CHANGE);
  enableInterrupt(RC_CH2_INPUT, calc_ch2, CHANGE);
 
}
 
void loop() {
 
  //Steering CH1   L 988 - 1980 R    C: 1480/1484
  //Throttle CH2   B 1350/1420 - 1982 F   C: 1472/1480/1484
 
  rc_read_values();
 
  Serial.print("CH1:"); Serial.print(rc_values[RC_CH1]); Serial.print("\t");
  Serial.print("CH2:"); Serial.println(rc_values[RC_CH2]);
 
  //delay(200);
 
  //int steeringMagnitude = 1482 - rc_values[RC_CH1];
 
  /*
  if (steeringMagnitude > RC_CH1_DZ) {
 
    int steeringBias =
   
  }
 
  */
 
  int throttleMagnitude = rc_values[RC_CH2] - 1486;
 
  if (throttleMagnitude > RC_CH2_DZ) {
    
    //if (steeringMagnitude > 
 
    //forward(motor1, motor2, map(throttleMagnitude, 10, 500, 0, 255));
    
    forward(motor1, motor2, map(throttleMagnitude, 10, 520, 0, 255));
    //forward(motor2, map(throttleMagnitude, 10, 520, 0, 255));
    
  } else if (throttleMagnitude < (0 - RC_CH2_DZ)) {
    
    back(motor1, motor2, map(throttleMagnitude, 10, 200, 0, 255));
    //back(motor2, map(throttleMagnitude, 10, 200, 0, 255));
   
  } else {
 
    brake(motor1, motor2);
   
  }

  int steeringMagnitude = rc_values[RC_CH1] - 1482;

  if (steeringMagnitude > RC_CH1_DZ && rc_values[RC_CH1] > 0) {

    right(motor1, motor2, map(steeringMagnitude, 10, 520, 0, 255));
    
  } else if (steeringMagnitude < (0 - RC_CH1_DZ)  && rc_values[RC_CH1] > 0) {

    left(motor1, motor2, map(steeringMagnitude, 10, 520, 0, 255));
    
  }
  
 
}
 
//Old Testing
/*
void loop()
{
   //Use of the drive function which takes as arguements the speed
   //and optional duration.  A negative speed will cause it to go
   //backwards.  Speed can be from -255 to 255.  Also use of the
   //brake function which takes no arguements.
   motor1.drive(255,1000);
   motor1.drive(-255,1000);
   motor1.brake();
   delay(1000);
   
   //Use of the drive function which takes as arguements the speed
   //and optional duration.  A negative speed will cause it to go
   //backwards.  Speed can be from -255 to 255.  Also use of the
   //brake function which takes no arguements.
   motor2.drive(255,1000);
   motor2.drive(-255,1000);
   motor2.brake();
   delay(1000);
   
   //Use of the forward function, which takes as arguements two motors
   //and optionally a speed.  If a negative number is used for speed
   //it will go backwards
   forward(motor1, motor2, 150);
   delay(1000);
   
   //Use of the back function, which takes as arguments two motors
   //and optionally a speed.  Either a positive number or a negative
   //number for speed will cause it to go backwards
   back(motor1, motor2, -150);
   delay(1000);
   
   //Use of the brake function which takes as arguments two motors.
   //Note that functions do not stop motors on their own.
   brake(motor1, motor2);
   delay(1000);
   
   //Use of the left and right functions which take as arguements two
   //motors and a speed.  This function turns both motors to move in
   //the appropriate direction.  For turning a single motor use drive.
   left(motor1, motor2, 100);
   delay(1000);
   right(motor1, motor2, 100);
   delay(1000);
   
   //Use of brake again.
   brake(motor1, motor2);
   delay(1000);
   
}
*/